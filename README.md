# Basic-J2se-Math-Class
Basic J2se Math Class
public final class Math
	 extends
     // Constants
+  public static final double E;
+  public static final double PI
	// Static methods

     public static int abs(int a);
     public static long abs(long a);
+  public static float abs(float a);
+  public static double abs(double a);
+  public static native double ceil(double a);
+  public static native double cos(double a);
+  public static native double floor(double a);
    public static int max(int a, long b);
    public static long max(long a, long b);
+  public static float max(float a, float b);
+  public static double max(double a, double b);
    public static int min(int a, int b);
    public static long min(long a, long b);
+ public static float min(float a, float b);
+ public static double min(double a, double b);
+ public static native double sin(double a);
+ public static native double sqrt(double a);
   public static native double tan(double angrad);
   public static double toDegrees(double angrad);
   public static double toRadians(double angdeg);
   }
